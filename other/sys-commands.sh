#list uuids and labels
sudo blkid

#list file system info
sudo fdisk -l



#users and groups
#[u:user g:group o:others a:all] [+:give -:take] [r:read w:write x:execute] [-R:recursive]
#ls -l list with permissions
sudo groupadd local-disk                #create a group
sudo usermod -a -G local-disk mustafa   #add an existing user to that group
sudo chown -R mustafa:local-disk vid/   #own a directory and its contents
sudo chmod g+rwx -R vid/                #give owners group rwx access
sudo chmod o-rwx -R vid/		#take rwx access from others

#gnome
gconf-editor
gconftool-2 --set --type bool /apps/nautilus/preferences/media_automount false


#git
git init
git add
git commit
git remote add origin ssh://repo.or.cz/srv/git/armadillo_firmware.git
git push --all origin

git clone git://repo.or.cz/armadillo_firmware.git
git branch -r  #access other branches
git remote show origin
git remote rm origin
git push origin master

git checkout -b mini2440-stable origin/mini2440-stable

wget -c #resume getting a partially-downloaded file
