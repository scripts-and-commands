#update previously installed packages
sudo apt-get update && sudo apt-get upgrade

#essential packages for a usable desktop environment
sudo ap-get install ubuntu-restricted-extras

#community maintained version of google chtome web browser
sudo add-apt-repository ppa:chromium-daily/ppa
sudo apt-get install chromium-browser

#Virtualization software. Package version must be updated
wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt-get install virtualbox-3.2

#wine
sudo add-apt-repository ppa:ubuntu-wine/ppa
sudo apt-get update
sudo apt-get install wine

#system 
sudo apt-get install gconf-editor kinfocenter gparted sensors-applet

#software development
sudo apt-get install eclipse glade git-core gitk subversion scons ssh
sudo apt-get install python3 python3-dev
sudo apt-get install subversion scons blender inkscape
sudo apt-get install libgmp3-dev libmpfr-dev build-essential zlib1g-dev libncurses5-dev patch texinfo libreadline5-dev


sudo apt-get install eagle openocd

#internet
sudo apt-get install vuze 

#multimedia
sudo apt-get install vlc kdenlive avidemux

#desktop
sudo apt-get install stellarium




